import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { LandingService } from 'src/app/services/landing.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-landing-form',
  templateUrl: './landing-form.component.html',
  styleUrls: ['./landing-form.component.css']
})
export class LandingFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();

  constructor(
    private readonly userService: UserService,
    private readonly landingService: LandingService
    ) { }

  public landingSubmit(loginForm: NgForm): void{
    //get username
    const {username} = loginForm.value;

    //console.log(username);

    this.landingService.login(username)
      .subscribe({
        next: (user: User) => {
            //redirect to catalogue page
            this.userService.user = user;
            this.login.emit();
        },
        error: () => {
          //handle error locally
        }
      })
  }
}
