import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const {apiUsers, apiKey} = environment;


@Injectable({
  providedIn: 'root'
})
export class LandingService {

  //Dependency injection.
  constructor(private readonly http: HttpClient) { }

  //Models, Observables and RxJS operators:

  //Login
  public login(username: string): Observable<User>{
    return this.checkUsername(username)
      .pipe(
        //switchmap: it allows us to switch to a different observable. Switch to creativeuser
        switchMap((user: User | undefined) => {
          if(user === undefined){ //if user does not exists
            return this.createUser(username);
          }
          return of(user);
        })
        
      )
  }


  //check if user exists
  private checkUsername(username: string): Observable<User | undefined>{
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(
        //RxJS Operators
        map((response: User[]) => response.pop())
      )
  }
  //Create a user
  private createUser(username: string): Observable<User>{
    //create user
    const user = {
      username,
      pokemon:[]
    };

    //headers -> API key
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });
    return this.http.post<User>(apiUsers, user, {
      headers
    })
  }

  //If user existed from login || user is created user -> store user
}
